using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Example : MonoBehaviour
{
    [Header("Example Reach Value")]
    public int reachValue;
    [Header("Data that will Change when reach")]
    public PlayerData data;

    [Header("Example Star Reward")]
    public RewardSystem<Reward> starReward;
    public List<RewardSystem<Reward>> moreStarReward;

    #region for 2019
    [Header("2019 or OLD")]
    public Custom_RewardSystem basicReward_2019;
    public List<Custom_RewardSystem> moreBasicStarReward_2019;

    [System.Serializable]
    public class Custom_RewardSystem : RewardSystem
    {
        public Custom_Reward[] _rewards;
        public override Reward[] pickRewards => _rewards;
    }

    [System.Serializable]
    public class Custom_Reward : Reward
    {
        [Header("Additional Var")]
        public int otherValue;
        public string someValue;
    }
    #endregion

    #region UI Control
    [Header("UI Section")]
    public Image[] starImages;
    public Text messege;
    #endregion



    private void Start()
    {
        int reachStarAmount = starReward.CheckReward(reachValue);
        messege.text += "Star Get Form Check Star : " + reachStarAmount;
        TurnStar(reachStarAmount);

        RewardSystemHelper.SneakPeekRewards<RewardSystem<Reward>, Reward>(pickReward =>
         {
             messege.text += "|Sneak Peek Requirement Value " +pickReward.rewardSystemId +" : "+ pickReward.checkValue;
         }, moreStarReward.ToArray());



        basicReward_2019.SneakPeekReward<Custom_Reward>(pickItem =>
        {
            Debug.Log("Pick Reward Value : " + pickItem.otherValue + " and " + pickItem.someValue);
        });

        RewardSystemHelper.SneakPeekRewards<Custom_RewardSystem, Custom_Reward>(moreBasicStarReward_2019.ToArray(), pickItem =>
         {
             Debug.Log("Check All avaiable Pick Reward : " + pickItem.otherValue + " and " + pickItem.someValue);
             Debug.Log("From Base :" + pickItem.rewardSystemId);
         });


    }

    void TurnStar(int turnAmount)
    {
        for (int i = 0; i < starImages.Length; i++)
        {
            if (turnAmount >= (i + 1))
            {
                starImages[i].gameObject.SetActive(true);
            }
            else
            {
                starImages[i].gameObject.SetActive(false);
            }
        }
    }

    [System.Serializable]
    public class PlayerData
    {
        public int gold;
        public float fame;
        public List<string> unlockedTittles;
    }
}