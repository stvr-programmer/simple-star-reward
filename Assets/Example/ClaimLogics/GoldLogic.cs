using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldLogic : ClaimLogic<int>
{
    public override void GetReward(int value)
    {
        Debug.Log("Gain Gold " + value);
        FindObjectOfType<Example>().data.gold += value;
    }
}
