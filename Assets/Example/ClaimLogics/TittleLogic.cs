using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TittleLogic : ClaimLogic<string>
{
    public override void GetReward(string value)
    {
        Debug.Log("Gain Tittle " + value);
        FindObjectOfType<Example>().data.unlockedTittles.Add(value);
    }
}
