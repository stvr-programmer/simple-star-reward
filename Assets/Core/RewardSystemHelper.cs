using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardSystemHelper
{
    public static void SneakPeekReward<Y, T>(Y rewardData, System.Action<T> pickReward) where T : Reward where Y : RewardSystem
    {
        rewardData.SneakPeekReward<T>(pickReward);
    }
    public static void SneakPeekRewards<Y, T>(Y[] rewardDatas, System.Action<T> pickReward) where T : Reward where Y : RewardSystem
    {
        for (int i = 0; i < rewardDatas.Length; i++)
        {
            rewardDatas[i].SneakPeekReward<T>(pickReward);
        }
    }

    public static void SneakPeekReward<Y, T>(System.Action<T> pickReward, Y rewardData) where T : Reward where Y : RewardSystem<T>
    {
        rewardData.SneakPeekReward(pickReward);
    }
    public static void SneakPeekRewards<Y, T>(System.Action<T> pickReward, Y[] rewardDatas) where T : Reward where Y : RewardSystem<T>
    {
        for (int i = 0; i < rewardDatas.Length; i++)
        {
            rewardDatas[i].SneakPeekReward(pickReward);
        }
    }

    public static void CheckReward<Y>(int inputValue, Y rewardData) where Y : RewardSystem
    {
        rewardData.CheckReward(inputValue);
    }
    public static void CheckReward<Y,T>(int inputValue, Y rewardData) where Y : RewardSystem<T> where T : Reward
    {
        rewardData.CheckReward(inputValue);
    }
    
    
}
