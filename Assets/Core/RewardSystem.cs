using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class RewardSystem<T> where T : Reward
{
    public string rewardId;
    [SerializeField] bool PickOnlyLatest = false;
    public bool pickOnlyLastest { get { return PickOnlyLatest; } }
    [SerializeField] T[] pickRewards;

    System.Action executeReward;
    public virtual int CheckReward(int inputCheckValue)
    {
        int checkReachAmount =0;
        executeReward = null;
        for (int i = 0; i < pickRewards.Length; i++)
        {
            if (inputCheckValue >= pickRewards[i].checkValue)
            {
                if (pickOnlyLastest) { executeReward = pickRewards[i].GetReward; }
                else { executeReward += pickRewards[i].GetReward; }

                checkReachAmount++;
            }
            else break;
        }
        executeReward?.Invoke();
        executeReward = null;
        return checkReachAmount;
    }
    public void SneakPeekReward(System.Action<T> avaiableReward)
    {
        for (int i = 0; i < pickRewards.Length; i++)
        {
            pickRewards[i].rewardSystemId = rewardId;
            avaiableReward?.Invoke(pickRewards[i]);
        }
    }
}
public class RewardSystem
{
    public string rewardId;
    [SerializeField] bool PickOnlyLatest = false;
    public bool pickOnlyLastest { get { return PickOnlyLatest; } }

    public virtual Reward[] pickRewards { get { return null; } }
    System.Action executeReward;
    public virtual void CheckReward(int inputCheckValue)
    {
        executeReward = null;
        for (int i = 0; i < pickRewards.Length; i++)
        {
            if (inputCheckValue >= pickRewards[i].checkValue)
            {
                if (pickOnlyLastest) { executeReward = pickRewards[i].GetReward; }
                else { executeReward += pickRewards[i].GetReward; }
            }
            else break;
        }
        executeReward?.Invoke();
        executeReward = null;
    }
    public void SneakPeekReward<T>(System.Action<T> avaiableReward) where T : Reward
    {
        for (int i = 0; i < pickRewards.Length; i++)
        {
            pickRewards[i].rewardSystemId = rewardId;
            avaiableReward?.Invoke((T)pickRewards[i]);
        }
    }
}


[System.Serializable]
public class Reward
{
    public int checkValue;
    public string rewardSystemId;
    [SerializeField] public UnityEvent claimRewardLogic;
    public void GetReward()
    {
        claimRewardLogic?.Invoke();
    }
}

